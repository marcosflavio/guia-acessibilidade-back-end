package com.guiaacessibilidade;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GuiaAcessibilidadeApplication {

	public static void main(String[] args) {
		SpringApplication.run(GuiaAcessibilidadeApplication.class, args);
	}
}
