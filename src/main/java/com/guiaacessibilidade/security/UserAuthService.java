package com.guiaacessibilidade.security;

import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.guiaacessibilidade.model.User;
import com.guiaacessibilidade.repository.IUserRepository;

public class UserAuthService implements UserDetailsService {

	private IUserRepository repository;

	public UserAuthService(IUserRepository repositoryParam) {
		this.repository = repositoryParam;
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = this.repository.findByUsername(username);
		if (user != null) {
			return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), true,
					true, true, true, AuthorityUtils.createAuthorityList("ADMIN"));
		} else {
			throw new UsernameNotFoundException("User not found");
		}
	}
}
