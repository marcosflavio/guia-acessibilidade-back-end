package com.guiaacessibilidade.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configurers.GlobalAuthenticationConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;

import com.guiaacessibilidade.repository.IUserRepository;

@Configuration
public class WebSecurity extends GlobalAuthenticationConfigurerAdapter {

	@Autowired
	private IUserRepository repository;

	@Override
	public void init(AuthenticationManagerBuilder auth) throws Exception {
		auth.inMemoryAuthentication().withUser("marcos").password("abc123").roles("ADMIN");
		auth.userDetailsService(getUserDetailsService());
	}

	private UserDetailsService getUserDetailsService() {
		return new UserAuthService(repository);
	}

}
