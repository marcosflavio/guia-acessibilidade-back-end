package com.guiaacessibilidade.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.guiaacessibilidade.model.Route;

public interface IRouteRepository extends MongoRepository<Route, String> {

	public List<Route> findByName(String name);
}
