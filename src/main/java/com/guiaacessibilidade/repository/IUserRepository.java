package com.guiaacessibilidade.repository;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.guiaacessibilidade.model.User;

public interface IUserRepository extends MongoRepository<User, String>{
	
	public User findByUsername(String username);

}
