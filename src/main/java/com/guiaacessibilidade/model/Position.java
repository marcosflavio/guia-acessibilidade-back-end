package com.guiaacessibilidade.model;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "positions")
public class Position {

	private Double lat;
	private Double lng;

	public Position() {
	}

	public Position(Double latParam, Double lngParam) {
		this.lat = latParam;
		this.lng = lngParam;
	}

	public Double getLat() {
		return lat;
	}

	public void setLat(Double lat) {
		this.lat = lat;
	}

	public Double getLng() {
		return lng;
	}

	public void setLng(Double lng) {
		this.lng = lng;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Position other = (Position) obj;
		if (lat == null) {
			if (other.lat != null)
				return false;
		} else if (!lat.equals(other.lat))
			return false;
		if (lng == null) {
			if (other.lng != null)
				return false;
		} else if (!lng.equals(other.lng))
			return false;
		return true;
	}
}
