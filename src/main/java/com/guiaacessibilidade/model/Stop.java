package com.guiaacessibilidade.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "stops")
public class Stop {

	@Id
	private String id;

	private Position position;
	private boolean isOrigin;
	private boolean isDestination;

	public Stop() {
		this.isDestination = false;
		this.isOrigin = false;
	}

	public Stop(Position positionParam) {
		this.position = positionParam;
		this.isOrigin = false;
		this.isDestination = false;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Position getPosition() {
		return position;
	}

	public void setPosition(Position position) {
		this.position = position;
	}

	public boolean isOrigin() {
		return isOrigin;
	}

	public void setOrigin(boolean isOrigin) {
		this.isOrigin = isOrigin;
	}

	public boolean isDestination() {
		return isDestination;
	}

	public void setDestination(boolean isDestination) {
		this.isDestination = isDestination;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Stop other = (Stop) obj;
		if (isDestination != other.isDestination)
			return false;
		if (isOrigin != other.isOrigin)
			return false;
		if (position == null) {
			if (other.position != null)
				return false;
		} else if (!position.equals(other.position))
			return false;
		return true;
	}
	
}
