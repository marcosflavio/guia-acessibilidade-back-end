package com.guiaacessibilidade.util;

import com.google.maps.DirectionsApi;
import com.google.maps.DirectionsApiRequest;
import com.google.maps.GeoApiContext;

public class GoogleRouteApi {
	private static final String API_KEY = "AIzaSyAuISKM0-PhsQ9UlRz0l11YuTEeURlo0po";

	private static GeoApiContext context;

	public static GeoApiContext getContext() {
		if (context == null) {
			context = new GeoApiContext().setApiKey(API_KEY);
		}
		return context;
	}

	public static DirectionsApiRequest newRequest() {
		return DirectionsApi.newRequest(getContext());
	}

}
