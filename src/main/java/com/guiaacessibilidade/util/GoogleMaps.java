package com.guiaacessibilidade.util;

import java.util.ArrayList;
import java.util.List;

import com.google.maps.DirectionsApiRequest;
import com.google.maps.model.DirectionsResult;
import com.google.maps.model.DirectionsRoute;
import com.google.maps.model.LatLng;
import com.guiaacessibilidade.model.Position;
import com.guiaacessibilidade.model.Route;
import com.guiaacessibilidade.model.Stop;

public class GoogleMaps {

	public static Route createRoute(Route route) {
		try {
			DirectionsApiRequest request = GoogleRouteApi.newRequest();
			request.origin(getOrigin(route.getStops()));
			request.destination(getDestiny(route.getStops()));
			request.waypoints(defineWayPoints(route.getStops()));
			DirectionsResult result = request.await();
			DirectionsRoute googleRoute = result.routes[0];
			List<Position> path = toPosition(googleRoute.overviewPolyline.decodePath());
			route.setPath(path);
			return route;

		} catch (Exception e) {
			throw new RuntimeException(e.getMessage());
		}
	}
	

	
	public static LatLng toLatLng(Position position) {
		return new LatLng(position.getLat(), position.getLng());
	}

	public static Position toPosition(LatLng coordinate) {
		return new Position(coordinate.lat, coordinate.lng);
	}

	public static List<Position> toPosition(List<LatLng> coordinateList) {
		List<Position> positionList = new ArrayList<>();
		for (LatLng coord : coordinateList) {
			positionList.add(toPosition(coord));
		}
		return positionList;
	}

	public static LatLng getOrigin(List<Stop> stopList) {
		stopList.get(0).setOrigin(true);
		return toLatLng(stopList.get(0).getPosition());
	}

	public static LatLng getDestiny(List<Stop> stopList) {
		int end = stopList.size() - 1;
		stopList.get(end).setDestination(true);
		return toLatLng(stopList.get(end).getPosition());
	}

	public static String[] defineWayPoints(List<Stop> stopList) {
		List<String> path = new ArrayList<>();
		for (LatLng coord : defineSubPoints(stopList)) {
			path.add(coord.toString());
		}
		return path.toArray(new String[path.size()]);
	}

	public static List<LatLng> defineSubPoints(List<Stop> stopList) {
		int end = stopList.size() - 1;
		List<LatLng> coordList = new ArrayList<>();
		for (Stop stop : stopList.subList(1, end)) {
			coordList.add(toLatLng(stop.getPosition()));
		}
		return coordList;
	}
}
