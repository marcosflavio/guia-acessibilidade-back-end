package com.guiaacessibilidade.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.guiaacessibilidade.model.User;
import com.guiaacessibilidade.service.UserService;

@CrossOrigin
@RestController
public class UserController {

	@Autowired
	private UserService service;

	@RequestMapping(value = "login", method = RequestMethod.POST)
	public User login(@RequestBody User requestUser) {
		return service.login(requestUser);
	}

	@RequestMapping(value = "signup", method = RequestMethod.POST)
	public User signUp(@RequestBody User requestUser) {
		return service.signup(requestUser);
	}
}
