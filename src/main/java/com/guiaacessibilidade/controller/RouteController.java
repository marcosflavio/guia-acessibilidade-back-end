package com.guiaacessibilidade.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.maps.model.GeocodingResult;
import com.guiaacessibilidade.model.Route;
import com.guiaacessibilidade.service.RouteService;

@CrossOrigin
@RestController
@RequestMapping(value = "/trixlog/routes")
public class RouteController {

	@Autowired
	private RouteService service;

	@RequestMapping(method = RequestMethod.GET)
	public List<Route> findAll() {
		return service.findAll();
	}

	@RequestMapping(method = RequestMethod.POST)
	public Route save(@RequestBody Route requestRoute) {
		return service.save(requestRoute);
	}
	
	@RequestMapping(value = "/search", method = RequestMethod.POST)
	public GeocodingResult[] search(@RequestBody String str) {
		return service.search(str);
	}
}
