package com.guiaacessibilidade.service;

import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.guiaacessibilidade.model.User;
import com.guiaacessibilidade.repository.IUserRepository;

@Transactional
@Service
public class UserService {

	@Autowired
	private IUserRepository repository;

	public User signup(User user) {
		User u = repository.findByUsername(user.getUsername());
		if (u == null) {
			user = repository.save(user);
			return user;
		}
		return null;
	}

	public User login(User user) {
		User userVar = repository.findByUsername(user.getUsername());
		if (userVar != null) {
			if (userVar.getPassword().equals(user.getPassword())) {
				return user;
			}
		}
		return null;
	}
}
