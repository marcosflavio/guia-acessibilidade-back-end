package com.guiaacessibilidade.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.maps.model.GeocodingResult;
import com.guiaacessibilidade.model.Route;
import com.guiaacessibilidade.model.Stop;
import com.guiaacessibilidade.repository.IRouteRepository;
import com.guiaacessibilidade.util.GeoGoogleAPI;
import com.guiaacessibilidade.util.GoogleMaps;

@Transactional
@Service
public class RouteService {

	@Autowired
	private IRouteRepository repository;

	public List<Route> findAll() {
		return repository.findAll();
	}

	public Route save(Route route) {
		route.setRouteDate(new Date());
		route.setStops(isTheSamePoint(route.getStops()));
		route = GoogleMaps.createRoute(route);
		return repository.save(route);
	}
	
	public GeocodingResult[] search (String str){
		GeoGoogleAPI api = new GeoGoogleAPI();
		return api.search(str); 
	}
	
	private List<Stop> isTheSamePoint(List<Stop> stops) {
		List<Stop> aux = new ArrayList<>();
		aux.add(stops.get(0));
		for (int i = 0; i < stops.size(); i++) {
			if (!aux.contains(stops.get(i))) {
				aux.add(stops.get(i));
			}
		}
		return aux;
	}
	
}
