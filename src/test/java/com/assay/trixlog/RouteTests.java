package com.assay.trixlog;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.fluent.Request;
import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.guiaacessibilidade.model.Position;
import com.guiaacessibilidade.model.Route;
import com.guiaacessibilidade.model.Stop;
import com.guiaacessibilidade.service.RouteService;

@RunWith(SpringRunner.class)
@SpringBootTest
@FixMethodOrder
public class RouteTests {

	@Autowired
	RouteService routeService;

	@Test
	public void getRoutesWithoutAuth() throws Exception {
		String port = System.getenv("PORT");
		if (port == null)
			port = "8080";
		Thread.sleep(2000);
		HttpResponse response = Request.Get("http://localhost:" + port + "/trixlog/routes").execute().returnResponse();
		Assert.assertEquals(401, response.getStatusLine().getStatusCode());
	}

	@Test
	public void testCreateRoute() {
		List<Stop> stops = new ArrayList<>();
		stops.add(new Stop(new Position(-3.700976d, -38.587072d)));
		stops.add(new Stop(new Position(-3.701158d, -38.583446d)));
		Date routeDate = new Date();
		Route route = new Route();
		route.setStops(stops);
		route.setRouteDate(routeDate);
		route.setName("Rota do Assert");
		route.setVehiclePlate("zzz-999");
		Route r = routeService.save(route);

		Assert.assertEquals(r.getId() != null, true);
	}
}
