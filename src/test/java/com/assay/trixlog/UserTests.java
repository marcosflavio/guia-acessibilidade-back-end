package com.assay.trixlog;

import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.guiaacessibilidade.model.User;
import com.guiaacessibilidade.service.UserService;

@RunWith(SpringRunner.class)
@SpringBootTest
@FixMethodOrder
public class UserTests {
	@Autowired
	private UserService userService;

	@Test
	public void testSignUpNewUser() {
		User user = new User("Marcosss", "marcos123");
		userService.signup(user);
		Assert.assertEquals(user.getId() != null, true);
	}

	@Test
	public void testSignUpExistingUser() {
		User user = new User("Daniel", "daniel");
		userService.signup(user);
		Assert.assertEquals(user.getId() == null, true);
	}

	@Test
	public void testLoginUser() {
		User u = new User("Daniel", "daniel");
		User user = userService.login(u);
		Assert.assertNotNull(user);
	}

}